package com.cinoy.cobacoba.controller;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity createCustomer(@RequestBody Addition addition) {
        return ResponseEntity.ok(addition);
    }
}

class Addition{
    public int a;
    public int b;
    public int res;

    public Addition(int a, int b){
        this.a = a;
        this.b = b;
        this.res = a+b;
    }

    public int getRes() {
        return res;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }
}
