package com.cinoy.cobacoba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CobaCobaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CobaCobaApplication.class, args);
	}

}
